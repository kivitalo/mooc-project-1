package sec.project.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "EVENT")
public class Event extends AbstractPersistable<Long> {

    private String name;
    private String description;
    private String urltoevent;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Signup> signupItems;
    
    public Event() {
        super();
    }

    public Event(String name, String description, String urltoevent) {
        this();
        this.name = name;
        this.description = description;
        this.urltoevent = urltoevent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   
    public void setUrltoevent(String urlname) {
        this.urltoevent = urlname;
    }
    
    public String getUrltoevent() {
        return urltoevent;
    }
    
    public List<Signup> getSignups() {
        return signupItems;
    }

    public void addSignup(Signup signupItem) {
        this.signupItems.add(signupItem);
    }
}
