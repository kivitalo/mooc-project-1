package sec.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sec.project.domain.Event;
import sec.project.domain.Signup;
import sec.project.repository.EventRepository;
import sec.project.repository.SignupRepository;

@Controller
public class SignupController {

    @Autowired
    private SignupRepository signupRepository;

    @Autowired
    private EventRepository eventRepository;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("events", eventRepository.findAll());
        return "index";
    }
    
/*    
    @RequestMapping("*")
    public String defaultMapping() {
        return "redirect:/form";
    }
*/
    @RequestMapping(value = "/form/", method = RequestMethod.GET)
    public String loadForm(Model model, @RequestParam Long id) {
        Event event = eventRepository.findById(id);
        model.addAttribute("event", event);
        return "form";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@RequestParam String name, @RequestParam String address,@RequestParam Long id) {
        Event event = eventRepository.findById(id);
        Signup signup = new Signup(name, address);
        event.addSignup(signup);
        eventRepository.save(event);        
        return "done";
    }
    
    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    public String list2(Model model) {
        model.addAttribute("events", eventRepository.findAll());
        return "manage";
    }

    @RequestMapping(value = "/manage/participants", method = RequestMethod.GET)
    public String showparticipants(Model model,@RequestParam Long id) {
        Event event = eventRepository.findById(id);
            model.addAttribute("signups", event.getSignups());
        return "participants";
    }

    @RequestMapping(value = "/manage/cancel", method = RequestMethod.GET)
    public String deleteevent(@RequestParam Long id) {
        eventRepository.delete(id   );
        return "redirect:/manage";
    }

    
    @RequestMapping(value = "/manage", method = RequestMethod.POST)
    public String add(@RequestParam String name, @RequestParam String description, @RequestParam String urltoevent) {
        if (name.trim().isEmpty() || description.trim().isEmpty() || urltoevent.trim().isEmpty()) {
            return "redirect:/manage";
        }
        // Sanitize the urltoevent string. Add more cases than below.
        if (urltoevent.trim().contains("redirect")) {
            return "redirect:/manage";
        }
            

        Event event = eventRepository.findByName(name);
        if (event == null) {
            event = new Event();
            event.setName(name);
            event.setDescription(description);         
            event.setUrltoevent(urltoevent);
            event = eventRepository.save(event);
        }
        
        return "redirect:/manage";
    }
    
}
